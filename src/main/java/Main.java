import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

/**
 * @author Vanja Novak
 * @version 1.0 28.06.2017 11:34
 */
public class Main {

    public static void main(String[] arg){
        ObjectMapper mapper = new ObjectMapper();

        Dto dto = new Dto("1","2");

        Map map = mapper.convertValue(dto, Map.class);

        System.out.println(map);

    }
}
